Il s'agit d'un projet scolaire et de mon premier projet gité. Veuillez faire preuve d'indulgence :)
Il y a quelques jeux en python, un conteneur avec une alpine déployée automatiquement et un snake sur une page html (le snake n'est pas de moi mais me vient de https://gist.github.com/straker/ff00b4b49669ad3dec890306d348adc4 et est sous licence CC0 1.0 Universal).
L'ensemble du projet est sous licence Unlicense (faites-en ce que vous voulez). 
Je ne garantis pas que tous les programmes fonctionnent.
