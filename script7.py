#!/usr/bin/env python3
import random

name = input("Quel est ton nom ? \n")
def concatenate(nom) :
  # Fonction qui concatène bonjour et le nom obtenu
  phrase = "Bonjour " + nom
  return phrase
print(concatenate(name))

age = input("Quel est ton âge ? \n")
if (int(age) < 0) : print ("Ton âge est négatif…")
elif (int(age) < 18) : print ("Tu es encore mineur·e, où sont tes parents ?")
elif (int(age) < 35) : print ("Tu es jeune, profites de la vie")
elif (int(age) < 60) : print ("Tu as encore de longues années devant toi")
else : print ("Prends bien soin de ta santé")
print ("Jouons ensemble à + ou - stp ! (quitte si tu ne veux pas)")
nombre = random.randint(1, 100)
#floor(random()*100+1)
hypothese = 0
print ("Je viens de choisir un nombre entre 1 et 100. Essaye de deviner")
while hypothese != nombre:
  hypothese = int(input("\n"))
  if (hypothese < 1) : print ("Mon nombre est compris entre 1 et 100, ton nombre est beaucoup trop petit, tu n'as pas bien compris la règle, recommence")
  elif (hypothese > 100) : print ("Mon nombre est compris entre 1 et 100, ton nombre est trop grand, tu n'as pas bien compris la règle, recommence")
  elif (hypothese < nombre) : print ("Mon nombre est plus grand, propose encore")
  else : print ("Mon nombre est plus petit, propose encore")
print ("Bravo, mon nombre est bien " + str(nombre))

#nombre = input("Choisis un nombre entre 1 et 100 \n. Je vais essayer de deviner")
#print ("50 ?"

